# To read the csv file & make the dataframe
import pandas as pd

# MySQL Credentials
import json

# For command line argument
import argparse

# for Connection with MySQL
import pymysql

# for Iteration inside the directory
import pathlib

parser = argparse.ArgumentParser("mysql_upload_file")

parser.add_argument('-c','--mysql_details',help="To read the configuration file")
parser.add_argument('-d','--source_dir',help="To read the source directory")
args = parser.parse_args()

f = open(args.mysql_details)

file = json.load(f)
print(file)

config = file["dataeaze"]

db = pymysql.connect(host=config["host"],user=config['username'],password=config["password"], database=config["database"])

cursor = db.cursor()

for p in pathlib.Path(args.source_dir).iterdir():
    if p.is_file():
        df = pd.read_csv(p)
        df = df.fillna(0)
        for index, row in df.iterrows():
            # print(row)
            # print(row[0],row[1],row[2],row[9])
            # break
            cursor.execute("insert into startup values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",(row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]))

    print("completed")


db.commit()
db.close()
f.close()
